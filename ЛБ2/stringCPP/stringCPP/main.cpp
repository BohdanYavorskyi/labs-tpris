#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int index(char);
char character(int);

int main() {
	setlocale(LC_CTYPE, "Ukrainian");

	string text = "������������_65";
	string gamma = "�������";
	string encrypted_text;
	int N = 44;

	encrypted_text.resize(text.size());
	for (int i = 0, j = 0; i<text.size(); i++, j++) {
		encrypted_text[i] = character((index(text[i]) + index(gamma[j])) % N);
		if (j == 6) j = 0;
	}

	cout << text << endl << gamma << endl;
	cout << encrypted_text << endl << endl;


	encrypted_text = "7���_����3�����5";
	gamma = "����������";

	text.resize(encrypted_text.size());
	for (int i = 0, j = 0; i<encrypted_text.size(); i++, j++) {
		text[i] = character((index(encrypted_text[i]) - index(gamma[j]) + N) % N);
		if (j == 6) j = 0;
	}

	cout << encrypted_text << endl << gamma << endl;
	cout << text << endl;

	return 0;
}

int index(char ch) {
	string alphabet = "���å�Ū��Ȳ���������������������_0123456789";
	for (int i = 0; i<alphabet.size(); i++) {
		if (alphabet[i] == ch) return i + 1;
	}
}

char character(int index) {
	string alphabet = "���å�Ū��Ȳ���������������������_0123456789";
	return alphabet[index - 1];
}





//#define _CRT_SECURE_NO_WARNINGS
//#include <iostream> 
//#include <conio.h>
//#include <fstream>
//#include <string>
//#include <time.h>
//#include <sstream>
//#include <iomanip>
//
//using namespace std;
//
//class Text
//{
//protected:
//	string text;
//public:
//	
//	Text()
//	{
//		text = "";
//	}
//
//	Text(string t)
//	{
//		text = t;
//	}
//
//	~Text() {}
//
//	//methods
//	void inputText();
//	string getText() { return text; }
//
//	string reverse(string);
//	void readFromFile(string);
//	void writeToFile(string, string);
//	
//	void checkExceptions(string);
//};
//
//void Text::inputText()
//{
//	string str;
//	cout << "Input a text: ";
//	getline(cin, str);
//
//	text = str;
//}
//
//string Text::reverse(string text) {
//	try
//	{
//		checkExceptions(text);
//
//		return (string(text.rbegin(), text.rend()));
//	}
//	catch (char* ex)
//	{
//		cout << ex << " in reverse()" << endl;
//		return "";
//	}
//}
//
//void Text::checkExceptions(string text){
//
//	if (text == "") {
//		throw ("\'The string is empty\' exception");
//	}
//	if (text.length() >= 1e+8)
//	{
//		throw ("\'The string is too large\' exception");
//	}	
//
//}
//
//void Text::readFromFile(string path)
//{
//	ifstream fin(path); // open for reading
//	string line, exeption;
//	text = "";
//
//	try {
//		if (!fin.is_open())
//		{
//			throw ("Read: file hasn't been found!");
//		}
//
//		//read from file
//		while (!fin.eof())
//		{
//			getline(fin, line);
//			text += line;
//			text += '\n';
//		}
//		fin.close();
//
//		checkExceptions(getText());
//
//		cout << "Text's been read!" << endl;
//	}
//	catch (char* msg){
//		cout << msg << " in readfromFile()" << endl;
//	}
//}
//
//void Text::writeToFile(string path, string text)
//{
//	try
//	{
//		checkExceptions(text);
//
//		//write into a file
//		ofstream out(path);
//		out << text;
//		out.close();
//
//		cout << "Text's been written into " << path << endl;
//	}
//	catch (char* ex)
//	{
//		cout << ex << " in writeToFile()" << endl;
//	}
//}
//
//int main()
//{
//	system("title TPRIS lab_2");
//	system("chcp 1251");
//	setlocale(LC_ALL, "Ukrainian");
//
//	clock_t start = clock();
//
//	Text t;
//	string temp, url = "D:/Texts/TPRIS/text.txt";
//	
//	t.readFromFile(url);
//	
//	cout << endl << "Text: " << t.getText() << endl;
//	cout << "Reversed text: " << t.reverse(t.getText()) << endl << endl;
//
//	//t.inputText();
//	temp = t.getText();
//
//	t.writeToFile("D:/Texts/TPRIS/rss.doc", t.reverse(t.getText()));
//
//	clock_t end = clock();
//	double time_spent = (double)(end - start);
//
//	cout << "\nTime of execution: " << time_spent << " milliseconds";
//
//
//	_getch();
//	return 0;
//}
