
package operationswithstrings;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Богдан
 */

class Text{
    private String text;

    public Text(String text){
        this.text = text;
    }
    
    public String getText(){
        return text;
    }

    public int getSize(){
        return text.length();
    }
    
    public String reverse(String str)
    {
        StringBuilder s = new StringBuilder(str);
        String res = String.valueOf(s.reverse());
        
        return res;
    }
  
    public void readText(String path)
    {
        try{
            String t = new String(Files.readAllBytes(Paths.get(path)), "windows-1251");
            this.text = t;
        }catch (NoSuchFileException ex){
            JOptionPane.showMessageDialog(null, "Incorrect file path!");
        } catch (IOException exc){
            JOptionPane.showMessageDialog(null, exc.getMessage());
        }
    }
    
    public void writeText(String path, String text){
        Exception ex = checkExceptions(text);
        if(ex != null){
            JOptionPane.showMessageDialog(null, ex.getMessage());
            return;
        }    
        
        try{
            FileWriter fw = new FileWriter(path);
            fw.write(text);

            JOptionPane.showMessageDialog(null, "Text's written.");
            
            fw.close();
        } catch(IOException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    public Exception checkExceptions(String text){
        if(text == null || "".equals(text))
            return new Exception("The string is empty");
        else if(text.length() >= 1e+8)
            return new Exception("The string is too large");
        
        return null;
    }
}

public class OperationsWithStrings {
    
    public static void main(String[] args) {
        
        //D:/Texts/TPRIS/text.txt
        
        long start = System.nanoTime();
        
        Scanner in = new Scanner(System.in);
       
        //System.out.println("Enter a string: ");
        //String text = in.nextLine();
        String text = "",
                path = "D:\\Texts\\TPRIS\\text.txt";
        
        Text t = new Text(text);
    
        //System.out.println(t.reverse(t.getText()));
        
        t.readText(path);
    
        String reversed = t.reverse(t.getText());
        
        System.out.println("Text: \n" + t.getText());
        System.out.println("Reversed text: \n" + reversed);
        
        t.writeText("D:\\Texts\\TPRIS\\rs.txt", reversed);
    
        long time = (System.nanoTime() - start) / 1000000;
        System.out.println("Time of execution: " + time + " milliseconds");
    }
}
