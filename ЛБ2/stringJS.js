document.getElementById("read").addEventListener('click', readFile);
document.getElementById("rev").addEventListener('click', reverse);
document.getElementById("write").addEventListener('click', writeToFile);
document.getElementById("reset").addEventListener('click', resetDiv);

function resetDiv(){
    document.getElementById('out').innerHTML = "";
}

function readFile() {
    //var file = object.files[0];
    var reader = new FileReader();

    file = document.getElementById('file').files[0];
    var size = file.size;
    
    reader.onload = function() {
        document.getElementById('out').innerHTML = reader.result;
    }
    reader.readAsText(file, 'windows-1251');
}

function writeToFile(object){
    var fso, f1;
    var divValue = document.getElementById('out').innerHTML;
    
    //var file = new File("D:\\res.txt", true);
    //file.open("a+");
    //file.writeln(str);
    //file.close();
    
    var fs = require("fs");
    var f = fs.writeFile("D:\\res.txt", divValue);
    
    //fso = new ActiveXObject("Scripting.FileSystemObject");
    //f1 = fso.CreateTextFile("D:\\res.txt", true);
    
    //f1.Write (divValue);
    //f1.Close();
}

function reverse(){
    
    var divValue = document.getElementById('out').innerHTML;
    var str = divValue;
    
    str = str.split("").reverse().join("");
    
    document.getElementById('out').innerHTML = str;
    return str;
}
