import sys
from datetime import datetime

class Text:
    def __init__(self, text=""):
        self.__text = text
        self.__size = len(text)

    def setText(self, text):
        self.__text = text
        self.__size = len(text)

    def getText(self):
        return self.__text

    def getSize(self):
        return self.__size

    def isEmpty(self):
        return self.getSize() == 0

#read from a file    
    def readFromFile(self, path):
        try:
            f = open(str(path), "r")
        except OSError as err:
            print("OS error: {0}".format(err))
        else:
            tempText = f.read()
            self.__text = tempText
            self.__size = len(tempText)
            f.close()
            
            if self.getSize() > 1e8:
                print("The string is too large")
                return ""
        
            print("Text's been read")

#write to file
    def writeToFile(self, path):
        f = open(str(path), 'w')
        f.write(str(self.getText()))
        f.close()
        print("Text's been written")

#reverse text
    def reverse(self):
        try:
            if self.isEmpty():
                raise Exception("Exception: 'The string is empty'")
        except Exception as err:
            return err
            
        reversed = self.getText()[::-1]
        return reversed

#main
#text = input ("\nInput a text: ")

t = Text()

fin = input ("\nInput a file path to be read from: ")

start_time = datetime.now()
t.readFromFile(fin)

print ("Text: \n", t.getText())
#print ("Size: ", t.getSize())
print ("Reversed text: \n", t.reverse())

end_time = datetime.now()
duration = end_time - start_time

fout = input("\nInput a file path to be written in: ")

start_time = datetime.now()
t.writeToFile(fout)
end_time = datetime.now()

duration += end_time - start_time

print('Duration: {}'.format(duration))

#print('Duration: {}'.format(end_time - start_time))
