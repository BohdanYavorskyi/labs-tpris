import React from 'react'
import * as vectorOperations from '../../server/vectors/main';
import SizeSelector from '../../containers/matrices/SizeSelector'
import { connect } from 'react-redux'

const VectorsForm = ({a, b, vectorSize, resVector}) => {

    let hr = (<div id={'hr'}></div>);
    let selectOperation = vectorOperations.createSelectOptions(+vectorSize);

    return(
        <div id="main">
            <span>Choose the size of vectors: </span>
            <SizeSelector />
            <div id="vector-container">{a}{hr}{b}{hr}</div>
            <div id="result-container">{selectOperation}{resVector}</div>
        </div>


    );
};

const mapStateToProps = state => ({
    vectorSize: state.vectors.vectorSize,
    resVector: state.vectors.resVector,
    a: state.vectors.a,
    b: state.vectors.b,
});

export default connect(
    mapStateToProps
)(VectorsForm)
