import React from 'react';
import CalculateExpression from '../../containers/calculator/CalculateExpression';
import ResultOfExpression from '../../containers/calculator/ResultOfExpression';

const ExpressionCalculatorForm  = () =>{
    return(
        <div id="main">
            <CalculateExpression/>
            <ResultOfExpression/>
        </div>
    );
}

export default ExpressionCalculatorForm;

