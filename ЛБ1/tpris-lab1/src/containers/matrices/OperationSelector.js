import { connect } from 'react-redux';

import * as matricesActions from '../../actions/matrices';
import * as vectorsActions from '../../actions/vectors';

import Select  from '../../components/matrices/Select';

const mapDispatchToProps = (dispatch) => ({
    onChange: (e) => {
        e.preventDefault();
        console.log(e.target.value);

        dispatch(matricesActions.setMatrixSize(e.target.value));
        dispatch(vectorsActions.setVectorSize(e.target.value));
    }
});

export default connect(
    null,
    mapDispatchToProps
)(Select)
