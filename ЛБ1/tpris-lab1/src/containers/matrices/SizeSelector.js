import { connect } from 'react-redux'

import * as matricesActions from '../../actions/matrices';
import * as vectorsActions from '../../actions/vectors';

import * as matricesApi from "../../server/matrices/main";
import * as vectorApi from "../../server/vectors/main";

import Select  from '../../components/matrices/Select';

import store from '../../index';

const mapDispatchToProps = (dispatch) => ({
    onChange: (e) => {
        e.preventDefault();

        const menu = store.getState().menu.activeItem;

        if(menu === 'Matrices'){
            let A = matricesApi.createMatrix(+e.target.value, 'matrix1', 'Matrix 1', false);
            let B = matricesApi.createMatrix(+e.target.value, 'matrix2', 'Matrix 2', false);

            dispatch(matricesActions.setMatrixSize(e.target.value));
            dispatch(matricesActions.createMatrixA(A));
            dispatch(matricesActions.createMatrixB(B));
            dispatch(matricesActions.calculateMatrices(null));
        } else if(menu === 'Vectors') {
            let a = vectorApi.createVector(+e.target.value, 'vector1', 'Vector 1 = ', false);
            let b = vectorApi.createVector(+e.target.value, 'vector2', 'Vector 2 = ', false);

            dispatch(vectorsActions.setVectorSize(e.target.value));
            dispatch(vectorsActions.createVectorA(a));
            dispatch(vectorsActions.createVectorB(b));
            dispatch(vectorsActions.calculateVectors(null));
        }
    }
});

export default connect(
    null,
    mapDispatchToProps
)(Select)
