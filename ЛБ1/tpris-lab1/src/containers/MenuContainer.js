import { connect } from 'react-redux';

import * as menuActions from '../actions/menu';
import * as expressionActions from '../actions/calculator';
//import * as matricesActions from '../actions/matrices';
//import * as vectorsActions from '../actions/vectors';

import Menu from '../components/Menu';

const openPage = evt => {
    let tablinks = document.getElementsByClassName("tablink");

    for (let i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
    }

    evt.currentTarget.className += " w3-red";
};

const mapDispatchToProps = dispatch => ({
    onClick: (e) => {
        e.preventDefault();

        openPage(e);
        const currentMenu = e.target.innerHTML.toString();
        dispatch(menuActions.activeItem(currentMenu));
        dispatch(expressionActions.calcExpression(null));

        /*if(currentMenu !== 'Matrices'){
            dispatch(matricesActions.setMatrixSize(0));
            dispatch(matricesActions.calculateMatrices(null));
            dispatch(matricesActions.createMatrixA(null));
            dispatch(matricesActions.createMatrixB(null));
        } else if(currentMenu !== 'Vectors'){
            dispatch(vectorsActions.setVectorSize(0));
            dispatch(vectorsActions.calculateVectors(null));
            dispatch(vectorsActions.createVectorA(null));
            dispatch(vectorsActions.createVectorB(null));
        }*/
    }
});

export default connect(
    null,
    mapDispatchToProps
)(Menu)
