import {connect} from "react-redux";
import MainContent from "../components/MainContent";

const mapStateToProps = state => ({
    activeItem: state.menu.activeItem
});

export default connect(
    mapStateToProps
)(MainContent)
