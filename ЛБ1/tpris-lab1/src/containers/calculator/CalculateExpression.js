import { connect } from 'react-redux';
import CalculateInput  from '../../components/calculator/CalculateInput';
import { calculateExpression, addResultOfExpressionToDB} from '../../server/calculator/calculateExpression'

import * as expressionActions from '../../actions/calculator';

const mapDispatchToProps = dispatch => ({
    onSubmit: (e, input) => {
        e.preventDefault();
        if (!input.value.trim()) {
            return
        }

        let result = calculateExpression(input.value);
        addResultOfExpressionToDB(input.value, result);
        dispatch(expressionActions.calcExpression(result));
    }
})

export default connect(
    null,
    mapDispatchToProps
)(CalculateInput)
