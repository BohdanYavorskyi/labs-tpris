import { connect } from 'react-redux'
import ExpressionResult from "../../components/calculator/ExpressionResult";

const mapStateToProps = state => ({
    expressionResult: state.expression.expressionResult
});

export default connect(
    mapStateToProps
)(ExpressionResult)
