import firebase from 'firebase';
require('firebase/firestore');

 // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDru90X73i5Jv48DzbE-dvWej883r1D2mg",
    authDomain: "lab1-ca4fa.firebaseapp.com",
    databaseURL: "https://lab1-ca4fa.firebaseio.com",
    projectId: "lab1-ca4fa",
    storageBucket: "lab1-ca4fa.appspot.com",
    messagingSenderId: "645180636346"
  };

  firebase.initializeApp(config);

  const settings = {timestampsInSnapshots: true};
  const db = firebase.firestore();
  db.settings(settings);

  export default db;