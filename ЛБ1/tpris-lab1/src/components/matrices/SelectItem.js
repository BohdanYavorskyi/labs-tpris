import React from 'react'

const SelectItem = ({text}) => {
    return (
        <option>
            {text}
        </option>
    );
}

export default SelectItem;