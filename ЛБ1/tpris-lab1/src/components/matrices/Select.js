import React from 'react'
import SelectItem from '../../components/matrices/SelectItem'

const Select = ({onChange}) => {

    const size = 50,
        arr = [];

    for(let i = 0; i < size; i++){
        arr.push(i+1);
    }

    return(
        <select onChange={(e) => onChange(e)}>
            {arr.map(item =>
                <SelectItem
                    key={item}
                    text={item}
                />
            )}
        </select>
    );
};

export default Select;

