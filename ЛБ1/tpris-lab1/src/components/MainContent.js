import React from 'react'

import ExpressionCalculatorForm from '../containers/forms/ExpressionCalculatorForm';
import MatricesForm from '../containers/forms/MatricesForm';
import VectorsForm from '../containers/forms/VectorsForm';

const MainContent = ({activeItem}) => {
    switch(activeItem){
        case 'Expression':
            return(
                <ExpressionCalculatorForm />
            );
        case 'Matrices':
            return(
                <MatricesForm />
            );
        case 'Vectors':
            return(
                <VectorsForm />
            );

        default:
            return(
                <div id={"main"}>
                    {activeItem}
                </div>
            );
    }
};

export default MainContent;

