import React from 'react'

import MenuContainer from '../containers/MenuContainer'
import MainContainer from "../containers/MainContainer";

const App = () => {
    return(
        <div>
            <MenuContainer/>
            <MainContainer />
        </div>
    );
}

export default App
