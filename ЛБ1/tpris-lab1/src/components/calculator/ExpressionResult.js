import React from 'react'

const ExpressionResult = ({expressionResult}) =>{
    return(
        <p className={'result-expression'}>Result for expression is: {expressionResult}</p>
    );
}

export default ExpressionResult;