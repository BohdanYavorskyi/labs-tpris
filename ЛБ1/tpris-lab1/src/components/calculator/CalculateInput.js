import React from 'react'

const CalculateInput = ({onSubmit}) => {
    let input;

    return (
        <div>
            <form onSubmit={(e) => onSubmit(e, input)}>
                <input
                    placeholder={'Input an expression'}
                    ref={node => input = node} />
                <button type="submit">
                    Calculate
                </button>
            </form>
        </div>
    )
}

export default CalculateInput