import React from 'react'

const Menu = ({onClick}) => {
    return(
        <div id={"menu"}>
            <p>Choose menu item to perform an operation</p>
            <div className="w3-bar w3-black">
                <button id="calculator"
                        className="w3-bar-item w3-button tablink"
                        onClick={(e) => onClick(e)}>Expression</button>
                <button id="matrices"
                        className="w3-bar-item w3-button tablink"
                        onClick={(e) => onClick(e)}>Matrices</button>
                <button id = "vectors"
                        className = "w3-bar-item w3-button tablink"
                        onClick = {(e) => onClick(e)}>Vectors</button>
            </div>
        </div>
    );
};

export default Menu;

