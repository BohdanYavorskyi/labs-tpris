const initialState = {
    expressionResult: null
};

const calcExpression = (state = initialState, action) => {
    switch (action.type) {
        case 'CALC_EXPRESSION':
            return {...state, expressionResult: action.expressionResult};

        default:
            return state
    }
}

export default calcExpression;