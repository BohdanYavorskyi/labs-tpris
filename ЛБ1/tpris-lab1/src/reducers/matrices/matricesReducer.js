const initialState = {
    matrixSize: '0',
    A: null,
    B: null,
    resMatrix: null,
};

const matrices = (state = initialState, action) => {
    switch (action.type) {
        case 'MATRIX_SIZE':
            return {...state, matrixSize: action.matrixSize};
        case 'CREATE_A':
            return {...state, A: action.A};
        case 'CREATE_B':
            return {...state, B: action.B};
        case 'CALC_MATRICES':
            return {...state, resMatrix: action.resMatrix};

        default:
            return state
    }
};

export default matrices;
