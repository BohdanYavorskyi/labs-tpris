import { combineReducers } from 'redux'
import expressionReducer from './calculator/expressionReducer';
import activeMenuItemReducer from './menu/activeMenuItemReducer';
import matricesReducer from './matrices/matricesReducer';
import vectorsReducer from './vectors/vectorsReducer';

const rootReducer = combineReducers({
    expression: expressionReducer,
    menu: activeMenuItemReducer,
    matrices: matricesReducer,
    vectors: vectorsReducer,
});

export default rootReducer;
