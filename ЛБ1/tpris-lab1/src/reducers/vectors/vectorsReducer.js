const initialState = {
    vectorSize: '0',
    a: null,
    b: null,
    resVector: null,
};

const vectors = (state = initialState, action) => {
    switch (action.type) {
        case 'VECTOR_SIZE':
            return {...state, vectorSize: action.vectorSize};
        case 'CREATE_VECTOR_A':
            return {...state, a: action.a};
        case 'CREATE_VECTOR_B':
            return {...state, b: action.b};
        case 'CALC_VECTORS':
            return {...state, resVector: action.resVector};

        default:
            return state
    }
};

export default vectors;
