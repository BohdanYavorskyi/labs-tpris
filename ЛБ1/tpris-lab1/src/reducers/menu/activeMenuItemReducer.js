
const initialState = {
    activeItem: 'Choose the menu item',
};

const activeMenuItem = (state = initialState, action) => {
    switch (action.type) {
        case 'ACTIVE_MENU_ITEM':
            return {...state, activeItem: action.activeItem};

        default:
            return state
    }
}

export default activeMenuItem