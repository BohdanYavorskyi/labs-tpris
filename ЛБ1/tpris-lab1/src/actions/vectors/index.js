
export const setVectorSize = vectorSize => ({
    type: 'VECTOR_SIZE',
    vectorSize: vectorSize
});

export const createVectorA = A => ({
    type: 'CREATE_VECTOR_A',
    a: A
});

export const createVectorB = B => ({
    type: 'CREATE_VECTOR_B',
    b: B
});

export const calculateVectors = resMatrix => ({
    type: 'CALC_VECTORS',
    resVector: resMatrix
});