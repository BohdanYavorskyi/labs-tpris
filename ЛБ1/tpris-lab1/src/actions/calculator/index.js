
export const calcExpression = expressionResult => ({
    type: 'CALC_EXPRESSION',
    expressionResult: expressionResult
});
