
export const activeItem = activeItem => ({
    type: 'ACTIVE_MENU_ITEM',
    activeItem: activeItem,
});
