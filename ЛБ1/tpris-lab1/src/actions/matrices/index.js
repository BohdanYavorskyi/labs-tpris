
export const setMatrixSize = matrixSize => ({
    type: 'MATRIX_SIZE',
    matrixSize: matrixSize
});

export const createMatrixA = A => ({
    type: 'CREATE_A',
    A: A
});

export const createMatrixB = B => ({
    type: 'CREATE_B',
    B: B
});

export const calculateMatrices = resMatrix => ({
    type: 'CALC_MATRICES',
    resMatrix: resMatrix
});