import { resultVectors, vectorToObject } from '../../vectors/main';
import * as vectorActions from '../../../actions/vectors';
import { addDataToDB } from "../../firestore/main";

import store from '../../../index';

const getVector = (id) => {
    const inputs = document.querySelectorAll(id);
    let matrix = [];

    for(let i=0; i<inputs.length; i++){
        matrix[i]= +inputs[i].value;
    }

    return matrix;
};

export const calculateVectors = (operation) => {
    const a = getVector('#vector1 input');
    const b = getVector('#vector2 input');

    const first = document.getElementById("first-mul").value;
    const second = document.getElementById("second-mul").value;
    let res = null;

    switch (operation){
        case '+':
            res = resultVectors(addVectors(b, a, first, second));
            store.dispatch(vectorActions.calculateVectors(res));
            break;
        case '-':
            res = resultVectors(subVectors(b, a, first, second));
            store.dispatch(vectorActions.calculateVectors(res));
            break;
        default:
            console.log('Incorrect');
            break;

    }
};

const addVectors = (a, b, first, second) =>{
    let size = a.length, c = [];
    let p = `${first}a + ${second}b = {`;

    for (let i = 0; i < size; i++) {
        p = p.concat(` ${first}·${a[i]} + ${second}·${b[i]}; `);
        c[i] = first*a[i] + second*b[i];
    }
    p = p.concat('} = { ');
    for (let i = 0; i < size; i++) {
        p = p.concat(`${c[i]}; `);
    }
    p = p.concat("}");

    addVectorsToDB(a, b, c, "Addition");
    return p;
};

const subVectors = (a, b, first, second) =>{
    let size = a.length, c = [];
    let p = `${first}a - ${second}b = {`;

    for (let i = 0; i < size; i++) {
        p = p.concat(` ${first}·${a[i]} - ${second}·${b[i]}; `);
        c[i] = a[i] + b[i];
    }
    p = p.concat('} = { ');
    for (let i = 0; i < size; i++) {
        p = p.concat(`${c[i]}; `);
    }
    p = p.concat("}");

    addVectorsToDB(a, b, c, "Substraction");

    return p;
};

const addVectorsToDB = (a, b, c, operation) =>{
    const obj = {
        a: vectorToObject(b),
        b: vectorToObject(b),
        Result: vectorToObject(c),
        Operation: operation,
    };

    addDataToDB('Vectors', obj);
};
