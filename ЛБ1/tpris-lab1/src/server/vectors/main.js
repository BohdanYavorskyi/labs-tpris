import React from 'react';
import * as calculateVectors from '../../server/vectors/calculations/main';
const uuidv4 = require('uuid/v4');

export const createVector = (size, id, className) => {
    if (size === 0){
        return;
    }

    let rows = [];

    for(let i = 0; i < size; i++) {
        let id = uuidv4();
        rows.push(
            <input
                key={id}
                type="number"
                defaultValue="0"
            />
        );
    }

    return(
        <div id={id}>{className}{"{"}{rows}<span>{"}"}</span></div>
    );
};

export const createSelectOptions = size => {
    if(size === 0){
        return;
    }

    return(
        <div>
            <span><input id="first-mul" type="number" defaultValue="1"/></span>
            <b><i> a<span style={{
                position: "relative",
                bottom: "1.0ex",
                letterSpacing: "-1.2ex",
                right: "1.0ex"}}
            >&ndash;</span> </i></b>
            <span>
                <select id="operation-select-vectors">
                    <option>+</option>
                    <option>-</option>
                </select>
            </span>
            <span> <input id="second-mul" type="number" defaultValue="1"/></span>
            <b><i>b<span style={{
                position: "relative",
                bottom: "1.2ex",
                letterSpacing: "-1.2ex",
                right: "1.0ex"}}
            >&ndash;</span></i></b>
            <button onClick={(e) => handleClick(e)}> = </button>
        </div>
    );
};

export const resultVectors = (result) => {
    return(
        <div>
            <p><b>Result: </b></p>
            <span><i>{result}</i></span>
        </div>
    );

};

export const vectorToObject = vector => {
    let obj = {};
    for(let i =0; i < vector.length; i++){
        obj[i] = vector[i];
    }

    return obj;
};

const handleClick = (e) => {
    e.preventDefault();
    const operation = document.getElementById("operation-select-vectors").value;
    calculateVectors.calculateVectors(operation);
};