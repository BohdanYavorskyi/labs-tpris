import React from 'react';
import { createMatrix, matrixToObject } from '../../matrices/main';
import * as calc from '../../../actions/matrices/index'
import store from '../../../index'
import { addDataToDB } from "../../firestore/main";

const getMatrix = (id) => {
    const inputs = document.querySelectorAll(id);
    let matrix = [];

    for(let i=0;i < inputs.length;i++){
        matrix.push([]);
    }

    for(let i=0; i<inputs.length; i++){
        for(let j=0; j < inputs[i].childNodes.length; j++){
            matrix[i][j]= +inputs[i].childNodes[j].value;
        }
    }

    return matrix;
};

const error = <div id="matrix3">Result: Invalid data</div>;

export const calculateMatrices = (operation) => {
    const A = getMatrix('#matrix1 div');
    const B = getMatrix('#matrix2 div');

    let C = [], res = null;

    switch (operation){
        case '+':
            C = addMatrix(A, B);
            res = C !== null ? createMatrix(C.length, 'matrix3', 'Result:', true, C) : error;
            store.dispatch(calc.calculateMatrices(res));
            break;
        case '-':
            C = subMatrix(A, B);
            res = C !== null ? createMatrix(C.length, 'matrix3', 'Result:', true, C) : error;
            store.dispatch(calc.calculateMatrices(res));
            break;
        case '*':
            C = mulMatrix(A, B, true);
            res = C !== null ? createMatrix(C.length, 'matrix3', 'Result:', true, C) : error;
            store.dispatch(calc.calculateMatrices(res));
            break;
        case '/':
            C = divMatrix(A, B);
            const cannot = <div id="matrix3">Result: Cannot divide matrices</div>;

            if(C === undefined){
                res = cannot;
            } else{
                res = C !== null ? createMatrix(C.length, 'matrix3', 'Result:', true, C) : error;
            }

            store.dispatch(calc.calculateMatrices(res));
            break;
        default:
            console.log('Incorrect');
            break;
    }
};

const isValidMatrices = (A, B) => {
    let size = A.length;

    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            if(j >= size/2 && (A[i][j] === 0 || B[i][j] === 0)){
                console.log("J: ", j);
                return false;
            }
        }
    }

    return true;
};

const addMatrix = (A, B) =>{
    let size = A.length, C = [];

    if(!isValidMatrices(A,B)){
        addMatricesToDB(A, B, "Invalid data", "Addition");
        return null;
    } else{
        for (let i = 0; i < size; i++) {
            C[i] = [];
            for (let j = 0; j < size; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }

        addMatricesToDB(A, B, matrixToObject(C), "Addition");

        return C;
    }
};

const subMatrix = (A, B) =>{
    let size = A.length, C = [];

    if(!isValidMatrices(A,B)){
        addMatricesToDB(A, B, "Invalid data", "Substraction");
        return null;
    } else{
        for (let i = 0; i < size; i++) {
            C[i] = [];
            for (let j = 0; j < size; j++) {
                C[i][j] = A[i][j] - B[i][j];
            }
        }

        addMatricesToDB(A, B, matrixToObject(C), "Substraction");

        return C;
    }
};

const mulMatrix = (A, B, add) =>{
    let size = A.length, C = [];

    if(!isValidMatrices(A,B)){
        if(add){
            addMatricesToDB(A, B, "Invalid data", "Multiplication");
        }
        return null;
    } else{
        for (let i = 0; i < size; i++){
            C[i] = [];
            for (let j = 0; j < size; j++){
                C[i][j] = 0;
                for (let k = 0; k < size; k++){
                    C[i][j] += A[i][k]*B[k][j];
                }
            }
        }

        if(add){
            addMatricesToDB(A, B, matrixToObject(C), "Multiplication");
        }

        return C;
    }
};

const divMatrix = (A, B) =>{
    let C = null;

    if(!isValidMatrices(A,B)){
        addMatricesToDB(A, B, "Invalid data", "Division");
        return C;
    } else{
        const inv = InverseMatrix(B);
        if(inv === false){
            addMatricesToDB(A, B, "Cannot divide matrices", "Division");
            return;
        } else{
            const C = mulMatrix(A,inv);
            addMatricesToDB(A, B, matrixToObject(C), "Division");
            return C;
        }
    }
};

const addMatricesToDB = (A, B, C, operation) =>{
    const obj = {
        A: matrixToObject(A),
        B: matrixToObject(B),
        Result: C,
        Operation: operation
    };

    addDataToDB('Matrices', obj);
};

function Determinant(A){
    let N = A.length, B = [], denom = 1, exchanges = 0;

    if(N === 1){
        return A[0][0];
    }

    for (let i = 0; i < N; ++i){
        B[i] = [];
        for (let j = 0; j < N; ++j){
            B[i][j] = A[i][j];
        }
    }

    for (let i = 0; i < N-1; ++i) {
        let maxN = i, maxValue = Math.abs(B[i][i]);
        for (let j = i+1; j < N; ++j){
            let value = Math.abs(B[j][i]);
            if (value > maxValue){
                maxN = j; maxValue = value;
            }
        }

        if(maxN > i){
            let temp = B[i]; B[i] = B[maxN]; B[maxN] = temp;
            ++exchanges;
        } else{
            if(maxValue === 0){
                return maxValue;
            }
        }

        let value1 = B[i][i];
        for (let j = i+1; j < N; ++j){
            let value2 = B[j][i];
            B[j][i] = 0;
            for (let k = i+1; k < N; ++k){
                B[j][k] = (B[j][k]*value1-B[i][k]*value2)/denom;
            }
        }
        denom = value1;
    }
    if (exchanges%2){
        return -B[N-1][N-1];
    } else{
        return B[N-1][N-1];
    }
}

function AdjugateMatrix(A) {
    let N = A.length, adjA = [];

    if(N === 1){
        return A;
    }

    for (let i = 0; i < N; i++)
    {
        adjA[i] = [];
        for (let j = 0; j < N; j++)
        {
            let B = [],
                sign = ((i+j)%2===0) ? 1 : -1;

            for (let m = 0; m < j; m++){
                B[m] = [];
                for (let n = 0; n < i; n++)   B[m][n] = A[m][n];
                for (let n = i+1; n < N; n++) B[m][n-1] = A[m][n];
            }
            for (let m = j+1; m < N; m++){
                B[m-1] = [];
                for (let n = 0; n < i; n++)   B[m-1][n] = A[m][n];
                for (let n = i+1; n < N; n++) B[m-1][n-1] = A[m][n];
            }
            adjA[i][j] = sign*Determinant(B);   // Функцию Determinant см. выше
        }
    }
    return adjA;
}

function InverseMatrix(matrixA){
    let det = Determinant(matrixA);
    if (det === 0){
        return false;
    }
    let N = matrixA.length, A = AdjugateMatrix(matrixA);

    for (let i = 0; i < N; i++){
        for (let j = 0; j < N; j++){
            A[i][j] /= det;
        }
    }
    return A;
}