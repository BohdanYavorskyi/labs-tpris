import React from 'react'
import * as calculateMatrices from '../../server/matrices/calculations/main'

const uuidv4 = require('uuid/v4');

export const createMatrix = (size, id, className, defaultValue, array) => {
    if (size === 0){
        return;
    }

    let rows = [],
        cols = [];

    for(let i = 0; i < size; i++) {
        for(let j = 0; j < size; j++) {
            let id = uuidv4();
            if(defaultValue){
                cols.push(
                    <input
                        key={id}
                        type="number"
                        value={array[i][j]}
                        readOnly="true"
                    />
                );
            } else{
                cols.push(
                    <input
                        key={id}
                        type="number"
                        defaultValue="0"
                    />
                );
            }
        }
        rows.push(<div>{cols}</div>);
        cols = [];
    }

    return(
        <div id={id}>{className}{rows}</div>
    );
};

export const createSelectOption = size => {
    if(size === 0){
        return;
    }

    return(
        <div>
            <span>Choose the operation </span>
            <select id="operation-select">
                <option>+</option>
                <option>-</option>
                <option>*</option>
                <option>/</option>
            </select>
            <button onClick={(e) => handleClick(e)}>Calculate</button>
        </div>
    );
};

export const matrixToObject = matrix => {
    let obj = {};
    for(let i =0;i<matrix.length;i++){
        obj[i]={}
    }
    for(let i =0;i<matrix.length;i++){
        for(let j =0;j<matrix[i].length;j++){
            obj[i][j]=matrix[i][j];
        }
    }

    return obj;
};

const handleClick = (e) => {
    e.preventDefault();
    const operation = document.getElementById('operation-select').value;
    calculateMatrices.calculateMatrices(operation);
};