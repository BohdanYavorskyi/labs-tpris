import db from "../../connector/connector";

const setData = function(docRef, obj){
    docRef.add(obj)
        .then(e => {
            window.alert("Information added to db");
        })
        .catch(error => {
            window.alert("An error occurred. Watch console.log()");
            console.error("Error: ", error);
        });
};

//set data to DB
export const addDataToDB = (collection, obj) => {
    const docRef = db.collection(collection);

    setData(docRef, obj);
};
