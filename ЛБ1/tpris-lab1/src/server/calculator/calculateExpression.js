import {addDataToDB} from "../firestore/main";

export  const calculateExpression = expression => {
	const error = 'Incorrect data';
	
	try{
		return eval(expression);
	} catch(err){
		return error;
	}
};

export const addResultOfExpressionToDB = (expression, result) =>{
    const obj = {
        Expression: expression,
        Result: result
    };

    addDataToDB('Expression', obj);
};