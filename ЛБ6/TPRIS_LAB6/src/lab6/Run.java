package lab6;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

class Task{
    private final int N;
    private final int start;

    public Task(int n, int start) {
        N = n;
        this.start = start;
    }

    public int getStart() {
        return start;
    }

    public int getN() {
        return N;
    }

    public double iterative(){
        double s = 1.;

        for(int x = getStart(); x <= getN(); x++){
            s *= ( x / ((Math.exp(x) - Math.pow(x, 2))));
        }

        return s;
    }

    public double recursive(double N){
        double s = 1.;

        if (N < 1){
            return s;
        } else {
            s *= (N / ((Math.exp(N) - Math.pow(N, 2))))*recursive(N-1);
        }

        return s;
    }

    public void addToFirestore(double iterative,
                               double recursive,
                               long timeIterative,
                               long timeRecursive){
        Firestore db = FirestoreClient.getFirestore();
        DocumentReference docRef = db.collection("LAB6").document();
        System.out.println("\nTrying to add with ID: " + docRef.getId());

        Map<String, Object> data = new HashMap<>();
        data.put("Operation", "Multiplication");
        data.put("Start value", getStart());
        data.put("N", getN());
        data.put("Iterative res", iterative);
        data.put("Recursive res", recursive);
        data.put("Iterative time", timeIterative);
        data.put("Recursive time", timeRecursive);
        ApiFuture<WriteResult> result = docRef.set(data);

        try {
            System.out.println("ADDED: " + result.get().getUpdateTime());
        } catch (InterruptedException e) {
            System.out.println("Exception: " + e.getMessage());
        } catch (ExecutionException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }
}

public class Run {
    static void initFirestore(){
        try (InputStream serviceAccount =
                     new FileInputStream("D:/КН-38/tpris-lab4-f409c3dc5276.json")){

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .build();

            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            System.out.println("Exception:" + e.getMessage());
        }
    }

    static void menu(){
        initFirestore();

        Task t = new Task(40, 1);

        System.out.println("Iterative: ");
        long startI = System.nanoTime();
        double iterative = t.iterative();
        long timeI = (System.nanoTime() - startI);

        System.out.println("Result: " + iterative);
        System.out.println("Time of execution: " + timeI + " nanoseconds");

        System.out.println("\nRecursive: ");
        long start = System.nanoTime();
        double recursive = t.recursive(t.getN());
        long time = (System.nanoTime() - start);

        System.out.println("Result: " + recursive);
        System.out.println("Time of execution: " + time + " nanoseconds");
        t.addToFirestore(iterative, recursive, timeI, time);
    }

    public static void main(String... args) {
        menu();
    }
}
