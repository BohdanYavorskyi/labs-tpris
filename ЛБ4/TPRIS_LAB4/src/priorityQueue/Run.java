package priorityQueue;


import java.io.*;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.*;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

/**
 *
 * @author Богдан
 */

public class Run {
    public static final String ADD = "Add data to queue";
    public static final String REMOVE = "Remove from the queue";
    public static final String SIZE  = "Size of the queue";
    public static final String HEAD = "Head of the queue";
    public static final String SHOW  = "Print the current queue";
    public static final String EXIT  = "Exit";
    public static final String ADDTODB  = "Add data to database";
    public static final String RETRIEVE  = "Retrieve data from database";

    public static <T> void menu(PrioQueue queue){

        try (InputStream serviceAccount =
                     new FileInputStream("D:/КН-38/tpris-lab4-f409c3dc5276.json")){

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .build();

            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            System.out.println("Exception:" + e.getMessage());
        }

        Firestore db = FirestoreClient.getFirestore();
        DocumentReference docRef = db.collection("Queue").document("PriorityQueue");

        System.out.println("\nMENU: ");
        System.out.println("1. " + ADD);
        System.out.println("2. " + REMOVE);
        System.out.println("3. " + HEAD);
        System.out.println("4. " + SIZE);
        System.out.println("5. " + SHOW);
        System.out.println("6. " + ADDTODB);
        System.out.println("7. " + RETRIEVE);
        System.out.println("0. " + EXIT);

        Scanner in = new Scanner(System.in);
        String choice;

        do{
            System.out.print("\nYour choice: ");
            choice = in.nextLine();

            switch(choice){
                case "1":
                    System.out.println("Input value and its priority: ");
                    System.out.print("Value: ");
                    Scanner K = new Scanner(System.in);
                    Scanner V = new Scanner(System.in);

                    String key = K.nextLine();
                    System.out.print("Priority: ");
                    Integer prio = V.nextInt();

                    queue.add((T)key, prio);
                    System.out.println("Element was added");
                    break;
                case "2":
                    Entry<T, Integer> set = queue.remove();
                    if(set == null){
                        System.out.println("Queue is empty");
                    } else{
                        System.out.println("Removed: " + set.getKey());
                    }
                    break;
                case "3":
                    Entry<T, Integer> head = queue.peek();
                    if(head == null){
                        System.out.println("Queue is empty");
                    } else{
                        System.out.println("Head: " + head.getKey());
                    }
                    break;
                case "4":
                    System.out.println("Size of the queue: " + queue.size());
                    break;
                case "5":
                    queue.show();
                    break;
                case "6":
                    queue.saveToFirestore(docRef);
                    break;
                case "7":
                    queue.retrieveFromFirestore(docRef);
                    break;
                case "0":
                    System.out.println("Exit...");
                    break;
                default:
                    System.out.println("Incorrect input!");
                    break;
            }
        } while (!choice.equals("0"));
    }

    public static void main(String... args) throws ExecutionException, InterruptedException {

        PrioQueue<String> queue = new PrioQueue();
        menu(queue);

        System.out.println("\nQueue after all changes: ");
        queue.show();
    }
}

