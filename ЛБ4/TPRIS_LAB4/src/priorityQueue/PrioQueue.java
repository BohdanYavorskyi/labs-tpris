package priorityQueue;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;

import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.WriteResult;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

    /**
     *
     * @author Богдан
     */

    class PrioQueue<T>{
        private List<Entry<T, Integer>> queue;

        PrioQueue(){
            queue = new ArrayList<>();
        }

        public boolean isEmpty(){
            return queue.isEmpty();
        }

        public int size(){
            return queue.size();
        }

        public void add(T element, int priority){
            Entry<T, Integer> set = new AbstractMap.SimpleEntry<>(element, priority);
            queue.add(set);

            Collections.sort(queue, Collections.reverseOrder(Entry.comparingByValue()));
        }

        public Entry<T, Integer> remove(){
            if(isEmpty()){
                return null;
            }

            //System.out.println("Removed " + queue.get(0).getKey());
            return queue.remove(0);
        }

        public Entry<T, Integer> peek(){
            if(isEmpty()){
                return null;
            }

            return queue.get(0);
        }

        public void show(){
            System.out.println("Output: ");
            if(isEmpty()){
                try {
                    throw new Exception("Queue is empty!");
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                    return;
                }
            }

            queue.forEach(e -> {
                T key = e.getKey();
                Integer priority = e.getValue();

                System.out.println(key.toString() + ": " + priority);
            });
        }

        public void saveToFirestore(DocumentReference docRef){
            if(isEmpty()){
                try {
                    throw new Exception("Queue is empty!");
                } catch (Exception ex) {
                    System.out.println("Exception: " + ex.getMessage());
                    return;
                }
            }

            Map<String, Object> data = new HashMap<>();
            for(int i = 0; i < size(); i++){
                data.put(String.valueOf(i+1),queue.get(i).getKey().toString() + " - " + queue.get(i).getValue());

                System.out.println("KEY: " + queue.get(i).getKey() +
                        "  VALUE: " + queue.get(i).getValue());
            }
            ApiFuture<WriteResult> result = docRef.set(data);

            try {
                System.out.println("ADDED: " + result.get().getUpdateTime());
            } catch (InterruptedException e) {
                System.out.println("Exception: " + e.getMessage());
            } catch (ExecutionException e) {
                System.out.println("Exception: " + e.getMessage());
            }
        }

        public void retrieveFromFirestore(DocumentReference docRef){
            // asynchronously retrieve the document
            ApiFuture<DocumentSnapshot> future = docRef.get();
            DocumentSnapshot document = null;

            try {
                document = future.get();
            } catch (InterruptedException e) {
                System.out.println("Exception: " + e.getMessage());
            } catch (ExecutionException e) {
                System.out.println("Exception: " + e.getMessage());
            }

            if (document.exists()) {
                queue.clear();

                document.getData().forEach( (key, value) -> {
                    String[] data = value.toString().split(" - ");
                    T element = (T) data[0];
                    Integer priority = Integer.parseInt(data[1]);

                    Entry<T, Integer> set =
                            new AbstractMap.SimpleEntry<>
                                    (element, priority);
                    queue.add(set);
                });
                System.out.println("State is restored");
            } else {
                System.out.println("No such document!");
                return;
            }
        }
    }
