package listAndTree;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.WriteResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

//Element in the tree
class Element{
    private String value;
    private Element parent;
    private ArrayList<Element> childs;

    Element(String value){
        this.value = value;
        this.parent = null;
        childs = new ArrayList<>();
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setParent(Element parent) {
        this.parent = parent;
    }

    public void setChilds(ArrayList<Element> childs) {
        this.childs = childs;
    }

    public void addChilds(ArrayList<Element> childs){
        if(getChilds() != null){
            this.childs.addAll(childs);
        } else{
            this.childs = childs;
        }
    }

    public String getValue() {
        return value;
    }

    public ArrayList<Element> getChilds() {
        return childs;
    }
}

//tree
class Tree {
    private final Element root;
    private ArrayList<Element> nodes;

    Tree(Element root){
        this.root = root;
        nodes = new ArrayList<>();
        nodes.add(root);
    }

    public Element getRoot() {
        return root;
    }

    public ArrayList<Element> getNodes() {
        return nodes;
    }

    private void addNodes(ArrayList<Element> newNodes){
        this.nodes.addAll(newNodes);
    }

    //add childs to parent
    private ArrayList<Element> addChilds(Element element){
        ArrayList<Element> childs = new ArrayList<>();
        Scanner in = new Scanner(System.in);

        System.out.print("Input childs for '" + element.getValue() + "': ");
        String inputed = in.nextLine();

        String[] childsArray = inputed.split(",");
        if(childsArray[0].equals("")){
            return null;
        }

        for(String i : childsArray){
            childs.add(new Element(i));
        }

        return childs;
    }

    private void createChildsFor(Element element){
        if(getRoot().getChilds() == null){
            return;
        }

        //forEach child in parent's element add childs if neccesary
        element.getChilds().stream().map((child) -> {
            this.nodes.add(child);
            child.setParent(element);
            child.setChilds(this.addChilds(child));
            return child;
        }).filter((child) -> {
            return !(child.getChilds() == null);
        }).forEachOrdered((child) -> {
            createChildsFor(child); //add new childs to parent's childs
        });
    }

    public void addNodesToParent(Element parent) {
        ArrayList<Element> newNodes = new ArrayList<>();

        getNodes()
                .stream()
                .filter(node -> node.getValue().equals(parent.getValue()))
                .forEachOrdered(node -> {
                    ArrayList<Element> c = addChilds(node);

                    node.addChilds(c);
                    newNodes.addAll(c);
                });

        if(newNodes.size() == 0){
            System.out.println("Nothing added");
        }
        this.nodes.addAll(newNodes);
    }

    public int removeNode(Element element){
        if(getNodes().isEmpty()){
            System.out.println("Tree is empty");
            return -1;
        }

        int count = 0;
        ArrayList<Element> toBeRemoved = new ArrayList<>();

        for (Element node : getNodes()) {
            if (node.getValue().equals(element.getValue())) {
                count++;
                toBeRemoved.add(node);
            }
        }

        getNodes().removeAll(toBeRemoved);
        if(count == 0){
            return 0;
        }
        return 1;
    }

    public void createTree(){
        root.setChilds(addChilds(root)); //add childs to root
        createChildsFor(root); //add more childs to the root's childs
    }

    public void show(){
        System.out.println("Tree:");
        if(getNodes().isEmpty()){
            System.out.println("Tree is empty");
            return;
        }

        getNodes().forEach( (node) -> {
            System.out.print("\n" + node.getValue() + " => ");

            if(node.getChilds() != null){
                node.getChilds().forEach( (child) -> {
                    System.out.print(child.getValue() + " ");
                });
            }
        });
        System.out.println();
    }

    //determine number of childs for each element in the tree
    public void numberOfChilds(){
        getNodes().forEach((node) -> {
            String value = node.getValue();
            int size = (node.getChilds() == null) ? 0 : node.getChilds().size();

            System.out.println("'" + value + "' has " + size + " childs");
        });
    }

    public void nodesWhereValueMoreThanRootBy1(){
        getNodes().stream().filter((node) -> {
            return node.getValue().length() - getRoot().getValue().length() == 1;
        }).forEachOrdered((node) -> {
            System.out.println("Element: " + node.getValue());
        });
    }

    public void saveToFirestore(DocumentReference docRef){
        if(getNodes().isEmpty()){
            try {
                throw new Exception("List is empty!");
            } catch (Exception ex) {
                System.out.println("Exception: " + ex.getMessage());
                return;
            }
        }

        System.out.println("Adding to firestore...");
        Map<String, Object> data = new HashMap<>();

        ArrayList<Element> childNodes;
        String childrenString;

        for(int i = 0; i < getNodes().size(); i++){
            childNodes = getNodes().get(i).getChilds();
            childrenString = getNodes().get(i).getValue() + " => ";

            if(childNodes != null) {
                for (Element child : childNodes) {
                    childrenString += child.getValue() + " ";
                }
            }

            data.put(String.valueOf(i+1), childrenString);
            System.out.println("Value: " + childrenString);
        }
        ApiFuture<WriteResult> result = docRef.set(data);

        try {
            System.out.println("ADDED: " + result.get().getUpdateTime());
        } catch (InterruptedException e) {
            System.out.println("Exception: " + e.getMessage());
        } catch (ExecutionException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }
}


