package listAndTree;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Богдан
 */

public class Run {
    public static final String ADD = "Add data to the list";
    public static final String REMOVE = "Remove from the list";
    public static final String SIZE  = "Size of the list";
    public static final String SHOW  = "Print the current list";
    public static final String EXIT  = "Exit";
    public static final String ADDTODB  = "Add data to database";
    public static final String RETRIEVE  = "Retrieve data from database";

    public static void initFirestore(){
        try (InputStream serviceAccount =
                     new FileInputStream("D:/КН-38/tpris-lab4-f409c3dc5276.json")){

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .build();

            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            System.out.println("Exception:" + e.getMessage());
        }
    }

    public static void listMenu(List list){
        Firestore db = FirestoreClient.getFirestore();
        DocumentReference docRef = db.collection("List").document("list");

        System.out.println("\nLIST MENU: ");
        System.out.println("1. " + ADD);
        System.out.println("2. " + REMOVE);
        System.out.println("3. " + SIZE);
        System.out.println("4. " + SHOW);
        System.out.println("5. " + ADDTODB);
        System.out.println("6. " + RETRIEVE);
        System.out.println("0. " + EXIT);

        Scanner in = new Scanner(System.in);
        String choice;

        do{
            System.out.print("\nYour choice: ");
            choice = in.nextLine();

            switch(choice){
                case "1":
                    System.out.println("Input an element and after element's value: ");
                    System.out.print("New element: ");
                    Scanner E = new Scanner(System.in);
                    Scanner A = new Scanner(System.in);

                    String element = E.nextLine();
                    System.out.print("Element's value to be inserted after: ");
                    String after = A.nextLine();

                    list.add(element, after);
                    break;
                case "2":
                    System.out.print("Remove element with value: ");
                    Scanner R = new Scanner(System.in);
                    String removing = R.nextLine();

                    boolean isRemoved = list.remove(removing);
                    if(isRemoved){
                        System.out.println("Element was removed");
                    } else{
                        System.out.println("Incorrect value");
                    }
                    break;
                case "3":
                    System.out.println("Size of the list: " + list.size());
                    break;
                case "4":
                    list.show();
                    break;
                case "5":
                    list.saveToFirestore(docRef);
                    break;
                case "6":
                    list.retrieveFromFirestore(docRef);
                    break;
                case "0":
                    System.out.println("Exit...");
                    break;
                default:
                    System.out.println("Incorrect input!");
                    break;
            }
        } while (!choice.equals("0"));
    }

    public static void treeMenu(Tree tree){
        Firestore db = FirestoreClient.getFirestore();
        DocumentReference docRef = db.collection("Tree").document("tree");

        System.out.println("\nTREE MENU: ");
        System.out.println("1. " + "Add childs to an element");
        System.out.println("2. " + "Remove the element from a tree");
        System.out.println("3. " + "Number of elements in the tree");
        System.out.println("4. " + "Number of childs for each element");
        System.out.println("5. " + "Print tree");
        System.out.println("6. " + ADDTODB);
        System.out.println("7. " + RETRIEVE);
        System.out.println("0. " + EXIT);

        Scanner in = new Scanner(System.in);
        String choice;

        System.out.println("\nInput childs using comma separator: ");
        tree.createTree();

        do{
            System.out.print("\nYour choice: ");
            choice = in.nextLine();

            switch(choice){
                case "1":
                    if(tree.getNodes().size() == 0){
                        tree.createTree();
                        break;
                    }

                    System.out.print("Parent value: ");
                    Scanner E = new Scanner(System.in);
                    String element = E.nextLine();

                    Element obj = new Element(element);
                    tree.addNodesToParent(obj);
                    break;
                case "2":
                    System.out.print("Remove elements with value: ");
                    Scanner R = new Scanner(System.in);
                    String removing = R.nextLine();

                    int isRemoved = tree.removeNode(new Element(removing));;
                    if(isRemoved == 1){
                        System.out.println("Element was removed");
                    } else if (isRemoved == 0){
                        System.out.println("Incorrect value");
                    }
                    break;
                case "3":
                    System.out.println("Number of elements in the tree: " + tree.getNodes().size());
                    break;
                case "4":
                    tree.numberOfChilds();
                    break;
                case "5":
                    tree.show();
                    break;
                case "6":
                    tree.saveToFirestore(docRef);
                    break;
                case "7":
                    //list.retrieveFromFirestore(docRef);
                    break;
                case "0":
                    System.out.println("Exit...");
                    break;
                default:
                    System.out.println("Incorrect input!");
                    break;
            }
        } while (!choice.equals("0"));
    }

    public static void main(String... args){
        initFirestore();

        List list = new List();
        Random ran = new Random();
        int value;

        for(int i = 0; i < 200; i++){
            value = ran.nextInt(100);
            list.list.add(String.valueOf(value));
        }

        ///listMenu(list);

        Element root = new Element("root");
        Tree tree = new Tree(root);

        tree.createTree();

        //treeMenu(tree);

        /*
        System.out.println("Input childs using comma separator: ");
        tree.createTree();

        System.out.println("\nResult: ");
        tree.numberOfChilds();
        */

        System.out.println("\nLab7: ");
        tree.nodesWhereValueMoreThanRootBy1();



    }
}
