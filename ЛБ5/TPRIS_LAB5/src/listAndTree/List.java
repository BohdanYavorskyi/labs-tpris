package listAndTree;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.WriteResult;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 *
 * @author Богдан
 */

class List {
    protected ArrayList<String> list;

    List(){
        list = new ArrayList<>();
    }

    private int getIndexForAdd(String after){
        return list.indexOf(after);
    }

    public void add(String element, String after){
        int index = getIndexForAdd(after);

        if(index == -1){
            try {
                throw new Exception("There is no element with value " + after);
            } catch (Exception e) {
                System.out.println("Exception: " + e.getMessage());
                return;
            }
        }

        list.add(index + 1, element);
        System.out.println("Element was added to the list");
    }

    public boolean remove(String value){
        return list.remove(value);
    }

    public int size(){
        return list.size();
    }

    public void show(){
        System.out.println("List:");
        if(list.isEmpty()){
            System.out.println("List is empty");
            return;
        }

        list.forEach(e -> {
            System.out.print(e + " ");
        });
        System.out.println();
    }

    public void saveToFirestore(DocumentReference docRef){
        if(list.isEmpty()){
            try {
                throw new Exception("List is empty!");
            } catch (Exception ex) {
                System.out.println("Exception: " + ex.getMessage());
                return;
            }
        }

        System.out.println("Adding to firestore...");
        Map<String, Object> data = new HashMap<>();
        for(int i = 0; i < size(); i++){
            data.put(String.valueOf(i+1), list.get(i));

            //System.out.println("Value: " + list.get(i));
        }
        ApiFuture<WriteResult> result = docRef.set(data);

        try {
            System.out.println("ADDED: " + result.get().getUpdateTime());
        } catch (InterruptedException e) {
            System.out.println("Exception: " + e.getMessage());
        } catch (ExecutionException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    public void retrieveFromFirestore(DocumentReference docRef) {
        ApiFuture<DocumentSnapshot> future = docRef.get();
        DocumentSnapshot document = null;

        try {
            document = future.get();
        } catch (InterruptedException e) {
            System.out.println("Exception: " + e.getMessage());
        } catch (ExecutionException e) {
            System.out.println("Exception: " + e.getMessage());
        }

        if (document.exists()) {
            System.out.println("Getting data from firestore...");
            list.clear();

            document.getData().forEach((key, value) -> {
                System.out.print(value.toString() + " ");
                list.add(value.toString());
            });
            System.out.println("\nState is restored");
        } else {
            System.out.println("No such document!");
            return;
        }
    }

}

