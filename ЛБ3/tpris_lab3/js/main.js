var db = firebase.firestore();

getInputedData = function(){
    const id = document.querySelector("#last_name").value;
    const _surname = id;
    const _name = document.querySelector("#name").value;
    const _team = document.querySelector("#team").value;
    const _sport = document.querySelector("#sport").value;
    const _place = +document.querySelector("#place").value;
    const _penalties = +document.querySelector("#penalties").value;
    
    obj = {
        surname: _surname,
        name: _name,
        team:{
            team: _team,
            sport: _sport
        },
        ranking:{
            place: _place,
            penalties: _penalties
        }
    };
    
    return obj;
}

setData = function(docRef, obj, str){
    docRef.set(obj).then(e => {
        window.alert(str);
        $("#add-data")[0].reset();
    })
    .catch(error => console.error("Error: ", error));
}

//set data to DB
addDataToDB = function(){    
    const id = document.querySelector("#last_name").value;
    const path = `Competition/${id}`;
    const docRef = db.doc(path);
    
    var obj = getInputedData();
    setData(docRef, obj, "Info was added");
}

//add
addData = function(){
    const id = document.querySelector("#last_name").value;
    const docRef = db.collection("Competition").doc(id);
    
    var obj = getInputedData();
    
    docRef.get().then( (doc) =>{
        if(doc && doc.exists){
            var update = window.confirm("The document already exist. UPDATE it?");
            
            if(update){
                setData(docRef, obj, "Information was updated!");
            }
        } else{
            addDataToDB();  
        }
    });
}

//return a field representaton in object
getDocumentKey = function(key){
    switch(key){
        case "surname":
            return "surname";
            break;
        case "name":
            return "name";
            break;
        case "team":
            return "team.team";
            break;
        case "sport":
            return "team.sport";
            break;
        case "place":
            return "ranking.place";
            break;
        case "penalties":
            return "ranking.penalties";
            break;
    }
}

//update select
updateFeildChanged = function(){
    const node = document.getElementById("upd-data-field");
    const value = node.options[node.selectedIndex].text;
    
    const valueInput = document.querySelector(".upd-data-field input[id='value']");
    const valueInputLabel = document.querySelector("label[for='upd-field']");
    
    valueInputLabel.innerHTML = `${value}`;
    valueInput.placeholder = `new ${value}`;
    valueInput.value = "";
   
    if(value === 'Place' || value === 'Penalties'){
        valueInput.attributes["type"].value = "number";
        valueInput.min = "0";
    } else{
        valueInput.attributes["type"].value = "text";
    }
}

//return an object (field + value)
dataForUpdate = function(){
    const id = document.querySelector(".upd-data-field input[id='id']").value;
    let value = document.querySelector(".upd-data-field input[id='value']").value;
    const fieldKey = document.getElementById("upd-data-field").value;
    
    if(fieldKey === 'place' || fieldKey === 'penalties'){
        value = +value;
    }
    
    const field = getDocumentKey(fieldKey);
    var newData = {
        [field]: value
    };
    
    return newData;
}

//update data by id
updateData = function(){
    const id = document.querySelector(".upd-data-field input[id='id']").value;
    const docRef = db.collection("Competition").doc(id);
      
    var obj = dataForUpdate();
    
    docRef.update(obj).then(function() {
        window.alert("Document successfully updated!");
        document.querySelector(".upd-data-field input[id='value']").value = "";
    })
    .catch(function(error) {
        window.alert("Incorrect ID.");
        //console.error("Error updating document: ", error);
    });
    
}

//remove doc by id
deleteData = function(){
    const id = document.querySelector("#del-data input[type='text']").value;
    const docRef = db.collection("Competition").doc(id);

    docRef.get().then( (doc) =>{
        if(doc && doc.exists){
            docRef.delete().then( (delDoc) => {
                window.alert("Document successfully deleted!");
            })
            .catch(error => console.error("Error removing document: ", error));
        } else{
            window.alert("Incorrect ID.");    
        }
    });          
}

//check IDs for comparison
validateComparison = function(value1, value2){
    resLabel = document.getElementById("cmp-res");
    
    if(value1 === undefined && value1 === undefined){
        resLabel.innerHTML = "Incorrect IDs.";
        return;
    } else if(value1 === undefined){
        resLabel.innerHTML = "Incorrect first object ID.";
        return;
    } else if(value2 === undefined){
        resLabel.innerHTML = "Incorrect second object ID.";
        return;
    } else{
        compareBy(value1,value2);
    }   
}

//compare by a field
compareBy = function(first, second){
    const key = document.getElementById("cmp-data-field").value;
    const field = getDocumentKey(key);
    
    var arr = [];
    let value1 = null;
    let value2 = null;
    
    if(field.includes(".")){ //if field is an object
        arr = field.split(".");
                    
        value1 = first[arr[0]][arr[1]];
        value2 = second[arr[0]][arr[1]];
    } else{
        value1 = first[field];
        value2 = second[field];    
    }
    
    if(value1 > value2){
        resLabel.innerHTML = `First object ${field} value is greater than second one.`;    
    } else if(value1 < value2) {
        resLabel.innerHTML = `Second object ${field} value is greater than first one.`;
    } else{
        resLabel.innerHTML = `Objects ${field} values are equal.`;
    }
}

compare = function(){
    const firstId = document.getElementById("first").value;
    const secondId = document.getElementById("second").value;    
    var docRef = db.collection("Competition");
    
    var firstDoc;
    var secondDoc;
        
    docRef.get().then(function(querySnapshot) {
        querySnapshot.forEach( doc => {
            if(doc.id === firstId){
                firstDoc = doc.data();
            } else if(doc.id === secondId){
                secondDoc = doc.data();
            }
        });     
        validateComparison(firstDoc, secondDoc);
    });   
}

//append data to the table
appendToTable = function(tableId, tbody, data){
    const table = document.getElementById(tableId);
    let tblBody = document.getElementById(tbody);
    let row = document.createElement("tr"); 

    //set rows
    let cell = document.createElement("td");
    let text = document.createTextNode(data.surname);
    cell.appendChild(text);
    row.appendChild(cell);
    
    cell = document.createElement("td");
    text = document.createTextNode(data.name);
    cell.appendChild(text);
    row.appendChild(cell);
    
    cell = document.createElement("td");
    text = document.createTextNode(data.team.team);
    cell.appendChild(text);
    row.appendChild(cell);
    
    cell = document.createElement("td");
    text = document.createTextNode(data.team.sport);
    cell.appendChild(text);
    row.appendChild(cell);
    
    cell = document.createElement("td");
    text = document.createTextNode(data.ranking.place);
    cell.appendChild(text);
    row.appendChild(cell);
    
    cell = document.createElement("td");
    text = document.createTextNode(data.ranking.penalties);
    cell.appendChild(text);
    row.appendChild(cell);
    
    //retrieve data 
    /*for (let key in data) {
        let cell = null;
        let text = null;

        if(key === "ranking" || key === "team"){
            for(let nested in data[key]){
                cell = document.createElement("td");
                text = document.createTextNode(data[key][nested]);
                cell.appendChild(text);
                row.appendChild(cell);
            }
        } else{
            cell = document.createElement("td");
            text = document.createTextNode(data[key]);
            cell.appendChild(text);
            row.appendChild(cell);
        }
    }*/
    
    tblBody.appendChild(row);
    table.appendChild(tblBody);
}

//show data
show = function(){
    $("tbody").children().remove(); //clear a body of the table 
    
    const table = document.getElementById("show-table");
    const caption = document.getElementById("show-caption");
    const showBar = document.getElementById("show-bar");
    
    const showMenu = document.querySelector("#nav-menu details:nth-of-type(6)");
    if(showMenu.value != open){ //prevent 2nd get process
        showMenu.value = open;
    } else{
        showMenu.value = "";
    }
        
    let count = 0;
    if(showMenu.value == open){
        caption.style.display = "none";    
        table.style.display = "none";

        //progress bar
        makeProgress(showBar);
        setTimeout(function(){
            caption.style.display = "block";
            table.style.display = "table";
        }, 5000);
        
        db.collection("Competition").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                if(doc && doc.exists){
                    appendToTable("show-table", "show-tbody", doc.data());
                    count++;
                    //console.log(`${doc.id} => ${doc.data().name}`);
                }
            });
            if(count === 0){
                setTimeout( f => {
                    window.alert("No result due to the current query.");
                }, 5000);    
            }
        })
        .catch(error => console.error("Error: ", error));    
    }
}

//search
searchFieldChanged = function(){
    const selected = document.getElementById("search-data-field").value;
    const node = document.getElementById("search-data-option");
        
    if(selected === "place" || selected === "penalties"){
        node.childNodes[1].style.display = "none";
        node.childNodes[3].style.display = "none";
        node.childNodes[5].style.display = "none";
        node.childNodes[9].style.display = "block";
        node.childNodes[13].style.display = "block";
        node.childNodes[7].selected = true;
    } else{
        node.childNodes[1].style.display = "block";
        node.childNodes[3].style.display = "block";
        node.childNodes[5].style.display = "block";
        node.childNodes[9].style.display = "none";
        node.childNodes[13].style.display = "none";
        node.childNodes[1].selected = true;    
    }   
}

//progress bar
makeProgress = function(bar){
    bar.style.display = "block";
    
    var width = 1;
    var id = setInterval(frame, 10);
    function frame() {
        if (width >= 95) {
            clearInterval(id);
        } else {
            width = width + 0.23; 
            bar.style.width = `${width}%`;
        }
    }
    
    //remove bar
    setTimeout(function(){
         bar.style.display = "none";
    }, 5000);
}

search = function(){
    $("tbody").children().remove(); //clear a body of the table 
    
    const table = document.getElementById("search-table");
    const caption = document.getElementById("search-caption");
    const searchBar = document.getElementById("search-bar");
    caption.style.display = "none";    
    table.style.display = "none";
    
    const field = document.getElementById("search-data-field").value;
    const key = getDocumentKey(field);
    const option = document.getElementById("search-data-option").value;
    let inputedValue = document.getElementById("srch-input").value;
    var isValidated;
    
    if(option === "contains" || option === "starts" || option === "ends"){
        isValidated = searchForStringField(key,option,inputedValue);
    } else{
        isValidated = searchForField(key,option,inputedValue);   
    }
    
    //console.log("ISVALIDATED: ",isValidated);
    
    if(isValidated){
        //progress bar
        makeProgress(searchBar);
        setTimeout(function(){
            caption.style.display = "block";
            table.style.display = "table";
        }, 5000);    
    }
}

searchForStringField = function(field,option,value){
    const docRef = db.collection("Competition");
    let arr = null;
    let count = 0;
    
    docRef.get()
        .then( querySnapshot => {
            querySnapshot.forEach( doc => {
                const data = doc.data();
                
                
                if(option === "starts"){ //got option
                    if(field.includes(".")){ //if field is an object
                        arr = field.split(".");
                        
                        if(data[arr[0]][arr[1]].startsWith(value)){
                            appendToTable("search-table", "search-tbl-body",data);
                            
                            count++;
                            //console.log(doc.id + "   " + data.team.team);
                        }
                    } else{ //field is not an object
                        if(data[field].startsWith(value)){
                            appendToTable("search-table", "search-tbl-body",data);
                            
                            count++;
                            //console.log(doc.id + "   " + data.team.team);
                        }    
                    }    
                } else if(option === "ends"){
                    if(field.includes(".")){
                        arr = field.split(".");
                        
                        if(data[arr[0]][arr[1]].endsWith(value)){
                            appendToTable("search-table","search-tbl-body", data);
                            
                            count++;
                            //console.log(doc.id + "   " + data.team.team);
                        }
                    } else{
                        if(data[field].endsWith(value)){
                            appendToTable("search-table", "search-tbl-body", data);
                            
                            count++;
                            //console.log(doc.id + "   " + data.team.team);
                        }    
                    }
                } else{
                     if(field.includes(".")){
                        arr = field.split(".");
                        
                        if(data[arr[0]][arr[1]].includes(value)){
                            appendToTable("search-table", "search-tbl-body", data);
                            
                            count++;
                            //console.log(doc.id + "   " + data.team.team);
                        }
                    } else{
                        if(data[field].includes(value)){
                            appendToTable("search-table", "search-tbl-body", data);
                            
                            count++;
                            //console.log(doc.id + "   " + data.team.team);
                        }    
                    }
                }  
            });
        
            if(count === 0){
                setTimeout( f => {
                    window.alert("No result due to the current query.");
                }, 5000);
            }
        })
        .catch( error => {
            console.error("Error getting documents: ", error);
        });
    
    return true;
}

searchForField = function(field,option,value){
    const docRef = db.collection("Competition");
    if(field === "ranking.place" || field === "ranking.penalties"){
        value = +value;
    }
    
    if(isNaN(value)){
        window.alert("Incorrect data type");
        return false;
    }
    
    let count = 0;
    var query = docRef.where(field, option, value);
    query.get()
        .then( querySnapshot => {
            querySnapshot.forEach( doc => {
                const data = doc.data();
                
                appendToTable("search-table", "search-tbl-body", data);
                
                count++;
                //console.log(doc.id + "   " + data.team.team);    
            });
            
            if(count == 0){
                setTimeout( f => {
                    window.alert("No result due to the current query.");
                }, 5000);
            }
        })
        .catch( error => {
            console.error("Error getting documents: ", error);
        });
    
    return true;
}

/*
*
//listeners
*
*/
//update data
const updBtn = document.getElementById("upd-data");
updBtn.addEventListener('submit', updateData);

//add data to db
const addBtn = document.getElementById("add-data");
addBtn.addEventListener('submit', addData);

//del from db
const delBtn = document.getElementById("del-data");
delBtn.addEventListener('submit', deleteData); 

//search
const searchBtn = document.getElementById("search");
searchBtn.addEventListener('submit', search); 

//compare data
const cmpBtn = document.getElementById("cmp-data");
cmpBtn.addEventListener('submit', compare); 

//show db
const showMenu = document.querySelector("#nav-menu details:nth-of-type(6) > summary");
showMenu.addEventListener('click', show);

//updSelect listener
const updSelect = document.getElementById("upd-data-field");
updSelect.addEventListener("change", updateFeildChanged);

//search field listener
const searchSelect = document.getElementById("search-data-field");
searchSelect.addEventListener("change", searchFieldChanged);





