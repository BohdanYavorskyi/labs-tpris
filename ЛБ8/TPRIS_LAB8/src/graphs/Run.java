package graphs;

public class Run {

    public static void main(String... args) {

        final int N = 40;
        Graph g = new Graph(N);

        g.show(g.getAdjacencyMatrix(), "Adjacency matrix");
        g.show(g.getIncidentMatrix(), "Incident matrix");

        System.out.println("\nDepth-first search: ");
        g.DFS(0);
    }
}