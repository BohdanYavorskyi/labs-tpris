package graphs;

import java.util.Random;

class Graph {
    private int[][] incidentMatrix;
    private int[][] adjacencyMatrix;
    final int N;
    private int[] used;

    Graph(int N){
        this.N = N;
        adjacencyMatrix = fillMatrix(N);
        used = new int[N];

        toIncidentMatrix();
    }

    Graph(int[][] adjacencyMatrix){
        this.adjacencyMatrix = adjacencyMatrix;
        this.N = adjacencyMatrix.length;
        used = new int[N];
    }

    public void setAdjacencyMatrix(int[][] adjacencyMatrix){
        this.adjacencyMatrix = adjacencyMatrix;
    }

    public int[][] getIncidentMatrix(){ return incidentMatrix; }

    public int[][] getAdjacencyMatrix(){
        return adjacencyMatrix;
    }

    public int getN(){
        return N;
    }

    public int[] getUsed(){
        return used;
    }

    public void show(int[][] matrix, String header){
        System.out.println(header);
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    private int getNumberOfRibs(){
        int ribs = 0;
        for (int i = 0; i < getN(); i++) {
            for (int j = 0; j < i; j++){
                if (getAdjacencyMatrix()[i][j] == 1){
                    ribs++;
                }
            }
        }

        return ribs;
    }

    private void toIncidentMatrix(){
        int ribs = getNumberOfRibs();
        int n = 0;
        this.incidentMatrix = new int[getN()][ribs];

        for (int i = 0; i < getN(); i++) {
            for (int j = 0; j < i; j++) {
                if (getAdjacencyMatrix()[i][j] == 1) {
                    this.incidentMatrix[i][n] = 1;
                    this.incidentMatrix[j][n] = 1;
                    n++;
                }
            }
        }
    }

    public void DFS(int v){
        this.used[v] = 1;
        System.out.println(v+1 + " is visited");

        for(int i = 0; i < getN(); i++){
            if(getUsed()[i] != 1 && getAdjacencyMatrix()[v][i] == 1){
                DFS(i);
            }
        }
    }

    private static int[][] fillMatrix(int N){
        int[][] matrix = new int[N][N];
        Random rand = new Random();

        for(int i = 0; i < N; i++){
            for(int j = i+1; j < N; j++){
                matrix[i][j] = rand.nextInt(2);
                matrix[j][i] = matrix[i][j];
            }
        }

        return matrix;
    }
}
